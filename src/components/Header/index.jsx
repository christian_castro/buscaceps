import { useHistory } from "react-router-dom"
import { Button } from "@material-ui/core"
import { ContainerHeader } from "./styles"
import { useAutenticacao } from "../../Providers/autenticacao"


export const Header = () => {

    const history = useHistory()
    const { setAutenticado } = useAutenticacao()

    const handleLogout = () => {
        setAutenticado(false)
        localStorage.removeItem("@BuscaCep/user")
        history.push("/")
    }

    return (
        <ContainerHeader>
            <Button style={{width: "9rem", height: "2.2rem"}} variant="contained" color="secondary" onClick={() => history.push("/dashboard")}>Buscar</Button>

            <Button style={{width: "9rem", height: "2.2rem"}} variant="contained" color="primary" onClick={() => history.push("/favorites")}>Favoritos</Button>
            
            <Button style={{backgroundColor: "orange", width: "9rem", height: "2.2rem"}} variant="contained" color="primary" onClick={handleLogout}>Logout</Button>
        </ContainerHeader>
    )
}